//
//  DetailViewController.swift
//  demoProject
//
//  Created by Mohamed anwar on 12/8/18.
//  Copyright © 2018 Mohamed anwar. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet var mainView: DetialView!
    
    var brand: Brand?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let newbrand = brand else { return }
        mainView.configure(brand: newbrand)
        
    }
    



}
