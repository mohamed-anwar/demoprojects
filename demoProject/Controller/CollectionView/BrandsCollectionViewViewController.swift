//
//  BrandsCollectionViewViewController.swift
//  demoProject
//
//  Created by Mohamed anwar on 12/8/18.
//  Copyright © 2018 Mohamed anwar. All rights reserved.
//

import UIKit

class BrandsCollectionViewViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate  {
    var arrOfBrands = [Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand(),Brand()]
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: "BrandCell", bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: "brandCell")
        
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return arrOfBrands.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "brandCell", for: indexPath) as! BrandCell
        let brand = arrOfBrands[indexPath.row]
    cell.confige(brand: brand)
        
    return cell
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let detailVC = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        let brand = arrOfBrands[indexPath.row]
        detailVC.brand = brand
        navigationController?.pushViewController(detailVC, animated: true)

    }
    
    

}
