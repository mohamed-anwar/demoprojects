//
//  Brand.swift
//  demoProject
//
//  Created by Mohamed anwar on 12/8/18.
//  Copyright © 2018 Mohamed anwar. All rights reserved.
//

import Foundation
import ObjectMapper

class Brand: Mappable {

    private var name: String!
    private var image: String!
    
    var _name: String{
        get {
            if name == nil {
                name = "BMW"
            }
            return name
        } set {
            name = newValue
        }
    }
    
    var _image: String {
        get {
            if image == nil {
                image = "https://hips.hearstapps.com/hmg-prod/images/2019-bmw-m5-competition-112-1533310555.jpg?crop=1xw:1xh;center,center&resize=900:*"
            }
            return image
        } set {
            image = newValue
        }
    }
    init() {
        
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        _name <- map["brandName"]
        _image <- map["brandImageUrl"]
    }
    
    
    
}
