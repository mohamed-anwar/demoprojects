//
//  BrandCell.swift
//  demoProject
//
//  Created by Mohamed anwar on 12/8/18.
//  Copyright © 2018 Mohamed anwar. All rights reserved.
//

import UIKit
import Kingfisher

class BrandCell: UICollectionViewCell {
    @IBOutlet weak var brandImageView: UIImageView!
    @IBOutlet weak var brandNameIabel: UILabel!
    var isViewSetup = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        if !isViewSetup {
            rounCornerLabel()
            isViewSetup = true
        }
    }
    
    func confige(brand: Brand){
        brandNameIabel.text = brand._name
        guard let url = URL(string: brand._image) else { return }
        brandImageView.kf.setImage(with: url)
    }
    
    func rounCornerLabel(){
        brandNameIabel.layer.cornerRadius = 12
        brandNameIabel.layer.masksToBounds = true
        brandNameIabel.clipsToBounds = true
    }

}
