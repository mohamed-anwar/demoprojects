//
//  DetialView.swift
//  demoProject
//
//  Created by Mohamed anwar on 12/8/18.
//  Copyright © 2018 Mohamed anwar. All rights reserved.
//

import UIKit
import Kingfisher
class DetialView: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(brand: Brand) {
        imageView.kf.setImage(with: URL(string: brand._image))
        label.text = brand._name
    }
    
}
